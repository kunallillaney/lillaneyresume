# Enter paper name here
PAPER = Kunal_Lillaney.tex
# Compile .tex file
LATEX = pdflatex -interaction=nonstopmode -shell-escape -file-line-error

all: clean $(PAPER).pdf

view: clean $(PAPER).pdf
	okular ./Kunal_Lillaney.pdf

$(PAPER).pdf:
	$(LATEX) $(PAPER)
	$(LATEX) $(PAPER)

clean:
	rm -rvf *.aux *.log *.bbl *.blg *.out *.lot *.pdf *.png *.dvi
